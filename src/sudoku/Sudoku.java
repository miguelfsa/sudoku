package sudoku;

import java.util.Scanner;

/**
 *
 * @author vitor.cunha
 */
public class Sudoku {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //1º ler a matriz
        int tamanho = 9;
        int[][] matrizSudoku = new int[tamanho][tamanho];
        boolean erroLinha, erroColuna, erroQuadrado;
        matrizSudoku = LerMatriz(tamanho);
        EscreverMatriz(matrizSudoku);

        //2º Validar a matriz
        // 2.1 -> Validar as linhas
        erroLinha = ValidarLinha(matrizSudoku);

        // 2.2 -> Validar as colunas
        erroColuna = ValidarColuna(matrizSudoku);

        // 2.3 -> Validar o 3x3
        erroQuadrado = ValidarQuadrados(matrizSudoku);
        
        //3º Enviar mensagem ao USER
        // if(erroLinha && erroColuna)
        if (erroLinha == true && erroColuna == true && erroQuadrado == true) {
            System.out.println("És um ás ao Sudoku!!!");
        } else {
            System.out.println("És ganda nabo!!");
        }
    }

    public static boolean ValidarLinha(int[][] matriz) {
        boolean tudoOk = true;
        int somaNumeros;
        for (int linhas = 0; linhas < matriz.length && tudoOk == true; linhas++) {
            
            somaNumeros = 0;
            for (int colunas = 0; colunas < matriz[linhas].length; colunas++) {
                
                somaNumeros = somaNumeros + matriz[linhas][colunas];
            }
            System.out.println("somaNumeros= Linha " + (linhas + 1)  + ": "+ somaNumeros);
            if (somaNumeros != 45) {
                tudoOk = false;
                return tudoOk;
            }
        }

        return tudoOk;
    }

    public static boolean ValidarColuna(int[][] matriz) {
        boolean tudoOk = true;
        int somaNumeros;
        for (int colunas = 0; colunas < matriz.length && tudoOk == true; colunas++) {
            somaNumeros = 0;
            for (int linhas = 0; linhas < matriz[colunas].length; linhas++) {
                somaNumeros = somaNumeros + matriz[linhas][colunas];
            }
            System.out.println("somaNumeros= Coluna " + (colunas + 1)  + ": "+ somaNumeros);
            if (somaNumeros != 45) {
                tudoOk = false;
                return tudoOk;
            }
        }

        return tudoOk;
    }
    
    public static boolean ValidarQuadrados(int [][] matriz){
        boolean tudoOk = true;
        int somaNumeros = 0;
        // teremos de validar sempre de 3 em 3
        // para isso teremos sempre de nos posicionar no vertice do quadrado ou seja linha ou coluna 1, 4 e 7 porque vamos validar em blocos:
        // Bloco 1:  LINHA ou COLUNA 1, 2, 3
        // Bloco 2:  LINHA ou COLUNA 4, 5, 6
        // Bloco 3:  LINHA ou COLUNA 7, 8, 9
        
        
        
        // criamos dois fors:
        // um para percorrer os blocos das linhas
        // outro para percorrer o bloco das colunas
        
        for (int posLinha = 0; posLinha < matriz.length - 2 && tudoOk == true; posLinha=posLinha+3) {
            // iremos fazer isto até completarmos todas as linhas e que a nossa variável de controlo não seja negativa 
            // para não estarmos a testar mais 3x3 caso algum não seja válido
            for (int posColuna = 0; posColuna < matriz[posLinha].length - 2; posColuna =posColuna+3) {
                
                // neste ponto sabemos que estamos no vértice [0,0] ou [0,3] ou [0,6] ou [3,0] ou [3,3] ou [3,6] ou [6,0] ou [6,3] ou [6,6]
                // logo é só ir de 3 em 3 e somar.
                
                for (int linha = 0; linha < 3; linha++) {
                    for (int coluna = 0; coluna < 3; coluna++) {
                        // criamos duas variaveis para cada indice da linha e coluna com base no valor que tivermos em posLinha ou posColuna
                        int linhaAtual = posLinha + linha;
                        int colunaAtual = posColuna + coluna;
                        
                        somaNumeros = somaNumeros + matriz[linhaAtual][colunaAtual];
                    }
                }
                
                System.out.println("Soma do 3x3 do bloco [" + posLinha + "," + posColuna + "] : " + somaNumeros);
                if(somaNumeros != 45){
                    tudoOk = false;
                }
                
                somaNumeros = 0;
                
                // agora é só validar e reiniciar a variavel somaNumeros
            }           
        }
        
        return tudoOk;
    }

    public static int[][] LerMatriz(int tamanho) {
        //1º ler a matriz
        // 1.1-> Ler a linha string
        // 1.2 -> separar numeros por virgula
        // 1.3 -> associar numeros lidos á matriz
        int[][] matrizLida = new int[tamanho][tamanho];
        // V 1.1-> Ler a linha string
        Scanner leituraDados = new Scanner(System.in);
        String linha = "";
        String[] separacaoLinha;

        for (int i = 0; i < tamanho; i++) {
            System.out.println("Dados da " + (i + 1) + "ª linha: ");
            //linha = leituraDados.nextLine();
            linha = DadosTeste(i);
            // 1.2 -> separar numeros por virgula
            separacaoLinha = linha.split(",");
            // 1.3 -> associar numeros lidos á matriz
            for (int indice = 0; indice < separacaoLinha.length; indice++) {
                matrizLida[i][indice] = Integer.parseInt(separacaoLinha[indice]);
            }
        }

        return matrizLida;
    }

    public static void EscreverMatriz(int[][] matrizLida) {
        for (int linhas = 0; linhas < matrizLida.length; linhas++) {
            System.out.print("Linha " + (linhas + 1) + ": ");
            for (int colunas = 0; colunas < matrizLida[linhas].length; colunas++) {
                System.out.print(matrizLida[linhas][colunas]);
            }

            System.out.println("");
        }
    }

    public static String DadosTeste(int linha) {
        String linhaRetorna = "";

        switch (linha) {
            case 0:
                linhaRetorna = "5,3,4,6,7,8,9,1,2";
                break;
            case 1:
                linhaRetorna = "6,7,2,1,9,5,3,4,8";
                break;
            case 2:
                linhaRetorna = "1,9,8,3,4,2,5,6,7";
                break;
            case 3:
                linhaRetorna = "8,5,9,7,6,1,4,2,3";
                break;
            case 4:
                linhaRetorna = "4,2,6,8,5,3,7,9,1";
                break;
            case 5:
                linhaRetorna = "7,1,3,9,2,4,8,5,6";
                break;
            case 6:
                linhaRetorna = "9,6,1,5,3,7,2,8,4";
                break;
            case 7:
                linhaRetorna = "2,8,7,4,1,9,6,3,5";
                break;
            case 8:
                linhaRetorna = "3,4,5,2,8,6,1,7,9";
                break;
            default:
                break;
        }

        return linhaRetorna;
    }

}
